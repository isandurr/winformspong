﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Csharp4WinFormsPong
{
    public partial class Form1 : Form
    {
        Player player1, player2;


        public Form1()
        {
            InitializeComponent();
            player1 = new Player(paddle1);
            player2 = new Player(paddle2);
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            player1.ProcessMove();
            player2.ProcessMove();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            CheckKeys(e, true);
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            CheckKeys(e, false);
        }

        private void CheckKeys(KeyEventArgs e, bool isDown)
        {
            switch (e.KeyCode)
            {
                case Keys.Oemcomma:
                case Keys.W:
                    player1.isUpPressed = isDown;
                    break;

                case Keys.O:
                case Keys.S:
                    player1.isDownPressed = isDown;
                    break;

                case Keys.Up:
                    player2.isUpPressed = isDown;
                    break;

                case Keys.Down:
                    player2.isDownPressed = isDown;
                    break;
            }
        }
    }
}