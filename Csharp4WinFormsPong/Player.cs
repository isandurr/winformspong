﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Csharp4WinFormsPong
{
    public class Player
    {
        const int movementSpeed = 3, topOfWorld = 0, bottomOfWorld = 444;

        public bool isUpPressed, isDownPressed;

        PictureBox paddle;
        bool? wasGoingUpLastTick;
        int numberOfTicksGoingInTheSameDirection;

        public Player(PictureBox paddle1)
        {
            this.paddle = paddle1;
        }

        internal void  ProcessMove()
        {
            bool? goingUp = null;

            if (isUpPressed)
            {
                goingUp = true;
            }
            if (isDownPressed)
            {
                if (goingUp.HasValue)
                {
                    goingUp = null;
                }
                else { goingUp = false; }
            }

            if (wasGoingUpLastTick.HasValue)
            {
                if (!goingUp.HasValue)
                {
                    wasGoingUpLastTick = null;
                    numberOfTicksGoingInTheSameDirection = 0;
                }
                else if (wasGoingUpLastTick.Value == goingUp.Value)
                {
                    numberOfTicksGoingInTheSameDirection++;
                }
                else
                {
                    wasGoingUpLastTick = goingUp;
                    numberOfTicksGoingInTheSameDirection = 1;
                }
            }
            else if (goingUp.HasValue)
            {
                wasGoingUpLastTick = goingUp;
                numberOfTicksGoingInTheSameDirection = 1;
            }

            DoMove(goingUp);
        }

        private void DoMove(bool? goingUp)
        {
            if (goingUp.HasValue)
            {
                var speed = (int)Math.Round(movementSpeed * ((float)numberOfTicksGoingInTheSameDirection / 10));
                if (goingUp.Value)
                {
                    speed *= -1;
                }
                paddle.Location = new Point(paddle.Location.X,
                    Math.Max(topOfWorld,
                    Math.Min(bottomOfWorld, paddle.Location.Y + speed)
                    )
                    );
            }
        }


    }
}
