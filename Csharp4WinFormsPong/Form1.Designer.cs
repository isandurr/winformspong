﻿namespace Csharp4WinFormsPong
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.paddle1 = new System.Windows.Forms.PictureBox();
            this.paddle2 = new System.Windows.Forms.PictureBox();
            this.ball = new System.Windows.Forms.PictureBox();
            this.timer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.paddle1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddle2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).BeginInit();
            this.SuspendLayout();
            // 
            // paddle1
            // 
            this.paddle1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paddle1.Image = ((System.Drawing.Image)(resources.GetObject("paddle1.Image")));
            this.paddle1.Location = new System.Drawing.Point(12, 232);
            this.paddle1.Name = "paddle1";
            this.paddle1.Size = new System.Drawing.Size(16, 103);
            this.paddle1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.paddle1.TabIndex = 0;
            this.paddle1.TabStop = false;
            // 
            // paddle2
            // 
            this.paddle2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.paddle2.Image = ((System.Drawing.Image)(resources.GetObject("paddle2.Image")));
            this.paddle2.Location = new System.Drawing.Point(801, 232);
            this.paddle2.Name = "paddle2";
            this.paddle2.Size = new System.Drawing.Size(16, 103);
            this.paddle2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.paddle2.TabIndex = 1;
            this.paddle2.TabStop = false;
            // 
            // ball
            // 
            this.ball.Image = ((System.Drawing.Image)(resources.GetObject("ball.Image")));
            this.ball.Location = new System.Drawing.Point(396, 284);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(100, 50);
            this.ball.TabIndex = 2;
            this.ball.TabStop = false;
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 1;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(829, 546);
            this.Controls.Add(this.ball);
            this.Controls.Add(this.paddle2);
            this.Controls.Add(this.paddle1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pong";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.paddle1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.paddle2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ball)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox paddle1;
        private System.Windows.Forms.PictureBox paddle2;
        private System.Windows.Forms.PictureBox ball;
        private System.Windows.Forms.Timer timer;
    }
}

